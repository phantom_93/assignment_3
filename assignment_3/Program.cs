﻿using System;
using System.Collections.Generic; 

namespace assignment_3
{
    class Program
    {
        static void Main(string[] args)
        {
            IList<string> stringList = new List<string>() {
                 "Anna Banana" ,
                 "Breaking Bad" ,
                "Anna Potato",
                "John Doe" ,
                "Jane Doe"
            };
            Console.WriteLine("Registered contacts are:");
            foreach (var item in stringList)
            {
                Console.WriteLine(item);
            }

            bool choice = true;
            while (choice)
            {
                Console.WriteLine("enter 'p' to continue and 'e'");
                Console.WriteLine("Enter a character/characters for partial matches. (p)");
                Console.WriteLine("Exit (e)");
                string menu = Console.ReadLine();
                switch (menu)
                {

                    case "p":
                        NameMatch(stringList);
                        break;
                    case "e":
                    default:
                        choice = false;
                        break;
                }
            }


        }


        public static void NameMatch(IList<string> stringList)
        {

            Console.WriteLine("Enter a character");
            string userCharacter = Console.ReadLine().ToLower();
            bool found = false;

            foreach (var str in stringList)
            {

                if (str.ToLower().Contains(userCharacter))
                {
                    Console.WriteLine($"contact found: ({str})");
                    found = true;
                }

            }

            if (found != true)
            {
                Console.WriteLine($"contact not found");

            }

        } // end of PartialMatch function



    }
}
